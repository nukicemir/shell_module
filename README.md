# shell_module



## Getting started

Shell module sample with goal to enable binary messages alongside Shell
#
## Interrupt-driven UART

Interrupt-driven UART implementation is based on the U-Blox's implementation of [UART library](https://github.com/u-blox/ubxlib/blob/80e76d49b97d8128202d5dd2a2fa9bfa2ecbc47a/port/platform/zephyr/src/u_port_uart.c#L290)
