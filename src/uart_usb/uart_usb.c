#include "uart_usb.h"
#include <zephyr/kernel.h>
#include <stdlib.h>
#include <zephyr/shell/shell.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/shell/shell_uart.h>


/* *************** UART defines *********************** */
struct uartData_t {
    int32_t     handle;
    char        *pData;
    uint16_t    len;
} uartData_t;

typedef struct{
    const struct device     *uartDev;
    struct k_fifo           fifoTxData;
    struct k_sem            txSem;
    uint8_t *               pBuffer;
    size_t                  receiveBufferSizeBytes;
    int32_t                 bufferWrite;
}port_uart_data_t;

/* *************** UART Data ************************* */
static port_uart_data_t m_uart_data = {
    .uartDev = DEVICE_DT_GET(DT_CHOSEN(zephyr_shell_uart)),
    .receiveBufferSizeBytes = UART_USB_RX_BUF_SIZE
};
/* **************************************************** */

static uart_usb_rx_isr_handler_t m_callback = NULL;

static void uart_usb_isr(const struct device *dev, void *user_data);

void uart_usb_enable(uart_usb_rx_isr_handler_t handler)
{
    if(handler){
        m_callback = handler;
        m_uart_data.pBuffer = k_malloc(m_uart_data.receiveBufferSizeBytes);
        m_uart_data.bufferWrite = 0;

        k_sem_init(&m_uart_data.txSem, 0, 1);
        k_fifo_init(&m_uart_data.fifoTxData);

        uart_irq_callback_user_data_set(m_uart_data.uartDev, uart_usb_isr, NULL);
        uart_irq_rx_enable(m_uart_data.uartDev);
    }else if(handler == NULL){
        m_callback = NULL;
        m_uart_data.bufferWrite = 0;
        k_free(m_uart_data.pBuffer);
        k_sem_reset(&m_uart_data.txSem);
    }
}

void uart_usb_tx(const uint8_t *tx_data, int size)
{
    struct uartData_t urtdata;
    urtdata.pData = (void *)tx_data;
    urtdata.len = size;

    k_fifo_put(&m_uart_data.fifoTxData, &urtdata);
    uart_irq_tx_enable(m_uart_data.uartDev);
    k_sem_take(&m_uart_data.txSem, K_FOREVER);
}

static void uart_usb_isr(const struct device *dev, void *user_data)
{
    static struct uartData_t    *pTxData;
    static uint32_t             txWritten;

    uart_irq_update(dev);

    if (uart_irq_rx_ready(dev)) {
        while(uart_fifo_read(dev, (m_uart_data.pBuffer + m_uart_data.bufferWrite), 1) != 0){
            m_uart_data.bufferWrite++;

            if(m_uart_data.bufferWrite == m_uart_data.receiveBufferSizeBytes){
                break; // exit while()
            }
        }

        if(m_callback){
            m_callback(m_uart_data.pBuffer, m_uart_data.bufferWrite);
            if(uart_irq_tx_ready(dev))
            {/* Echo */
                while(txWritten < m_uart_data.bufferWrite)
                {
                    txWritten += uart_fifo_fill(dev, (m_uart_data.pBuffer + txWritten), (m_uart_data.bufferWrite - txWritten));
                }
                txWritten = 0;
            }
        }
        m_uart_data.bufferWrite = 0;
    }

    if(uart_irq_tx_ready(dev)){
        if(pTxData == NULL){
            pTxData = k_fifo_get(&m_uart_data.fifoTxData, K_NO_WAIT);
            txWritten = 0;
        }

        if(!pTxData){
            uart_irq_tx_disable(m_uart_data.uartDev);
            return;
        }

        if(pTxData->len > txWritten){
            txWritten += uart_fifo_fill(m_uart_data.uartDev, pTxData->pData + txWritten, pTxData->len - txWritten);
        }
        else{
            pTxData = NULL;
            txWritten = 0;
            k_sem_give(&m_uart_data.txSem);

            if(k_fifo_is_empty(&m_uart_data.fifoTxData)){
                uart_irq_tx_disable(m_uart_data.uartDev);
            }
        }
    }
}
