/**
 * @file
 * @brief UART USB operations
 */


#ifndef UART_USB_H_
#define UART_USB_H_

#include <zephyr/kernel.h>
#include <stdlib.h>
#include <zephyr/shell/shell.h>

#define UART_USB_RX_BUF_SIZE            200

typedef void (*uart_usb_rx_isr_handler_t )(const uint8_t *rx_data, int size);

void uart_usb_enable(uart_usb_rx_isr_handler_t handler);

void uart_usb_tx(const uint8_t *tx_data, int size);


#endif // UART_USB_H_
